import { Task} from './task'
import * as moment from 'moment';
moment.locale('ru')
 export const ALL_TASKS: Task[] = [
     {name: 'task-1', description: 'description-1', data: moment("16.02.2018","DD.MM.YYYY").format('L')},
     {name: 'task-2', description: 'description-2', data: moment("19.02.2018","DD.MM.YYYY").format('L') },
     {name: 'task-3', description: 'description-3', data: moment("12.02.2017","DD.MM.YYYY").format('L') },
     {name: 'task-4', description: 'description-4', data: moment("12.03.2018","DD.MM.YYYY").format('L') }
 ]