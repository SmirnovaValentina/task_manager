import { Component, OnInit } from '@angular/core';
import { Task} from '../task';
import * as moment from 'moment'
import { ALL_TASKS} from '../all-tasks'

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
tasks = ALL_TASKS;
selected: Task;

openForm: boolean;
openDesc: boolean;

onSelect(task: Task):void {
  this.selected = task;
}
shodDesc(task: Task):void {
  let desc = document.getElementById(task.name).style.visibility="visible"; 
  this.openDesc = true;
}
closeDesc(task:Task) {
  let desc = document.getElementById(task.name).style.visibility="hidden"; 
  this.openDesc = false;  
}
innerT():void{
}
edit(val:boolean,task:Task): void{
  if(val === true) {
    this.openForm = true;
    this.selected = task;
  } else {
    this.openForm = false;
    this.selected = null;
  }
}
  constructor() { }

  ngOnInit() {
  }

  onFormSubmit(form:any): void {
    console.log(form);
    this.tasks.push(form);
  }

}
